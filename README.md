# Sainsburys screening test

## Requirements
* Docker engine >= 1.9 (tested with 1.10)
* Docker compose >= 1.5 (tested with 1.6) (https://docs.docker.com/compose/install/)

## Running it
`docker-compose up -d` and hit the browser on http://localhost

## Cleaning it up
`docker-compose stop && docker-compose rm`
