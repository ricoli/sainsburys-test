FROM golang:1.5-alpine

COPY app.go $GOPATH/
EXPOSE 8484
CMD ["go","run","app.go"]
